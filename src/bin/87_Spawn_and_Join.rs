use std::thread;
use std::thread::sleep;

fn main() {
    let handle = thread::spawn(move || {
        println!("Hello from a thread!");
    });
    thread::sleep(std::time::Duration::from_millis(1000));

    let v = vec![1, 2, 3];
    let handle2 = thread::spawn(move || {
        println!("Here's a vector: {:?}", v);
    });
    thread::sleep(std::time::Duration::from_millis(1000));

    println!("Main thread");
    let v = vec![1, 2, 3];
    let mut thread_handles = vec![];
    for e in v {
        let handle = thread::spawn(move || {
            println!("Here's a vector: {:?}", e);
        });
        thread_handles.push(handle);
    }
    for handle in thread_handles {
        handle.join().unwrap();
    }
}