fn return_bool(true_or_false: bool) -> bool {
    match true_or_false {
        true => true,
        false => false,
    }
}

fn main() {
    let true_or_false = true;
    let result = return_bool(true_or_false);
    println!("{}", result);
}