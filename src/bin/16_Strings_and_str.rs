fn main() {
    let name = String::from("Peter");
    let course = "Rust".to_string();
    let name2 = name.replace("Peter", "Kevin");
    println!("name: {}, course: {}, name2: {}", name, course, name2);
    let str1 = "Hello";
    let str2 = str1.clone();
    let str3 = &str2;
    println!("str1: {}, str2: {}, str3: {}", str1, str2, str3);
    println!("{}", "one".to_uppercase() == "ONE");
}