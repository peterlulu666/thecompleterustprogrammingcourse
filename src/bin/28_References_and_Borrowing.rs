fn change_string(some_string: &mut String) {
    some_string.push_str(", world");
}
fn main() {
    let mut s = String::from("hello");
    change_string(&mut s);
    println!("{}", s);
}