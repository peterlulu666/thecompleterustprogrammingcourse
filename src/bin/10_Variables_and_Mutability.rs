fn main() {
    let mut x = 5;
    println!("The value of x is: {}", x);
    x = 6;
    println!("The value of x is: {}", x);
    const SECOND: u32 = 1;
    println!("The value of SECOND is: {}", SECOND);
}