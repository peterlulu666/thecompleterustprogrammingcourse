use std::collections::BinaryHeap;

fn main() {
    let mut heap = BinaryHeap::new();
    heap.push(1);
    heap.push(2);
    heap.push(3);
    println!("{:?}", heap);
    heap.pop();
    println!("{:?}", heap);
    println!("{:?}", heap.peek());
    println!("{:?}", heap);
}