use std::collections::HashMap;

fn main() {
    let mut hash_map = HashMap::new();
    hash_map.insert(1, 1);
    hash_map.insert(5, 2);
    hash_map.insert(30, 3);
    println!("{:?}", hash_map);
    println!("{:?}", hash_map.contains_key(&1));
    println!("{:?}", hash_map.contains_key(&10));
    println!("{:?}", hash_map.get(&1));
    println!("{:?}", hash_map.get(&10));
    let remove_key = hash_map.remove(&1);
    println!("{:?}", remove_key);
    println!("{:?}", hash_map);
    let remove_entry = hash_map.remove_entry(&5);
    println!("{:?}", remove_entry);
    println!("{:?}", hash_map);
    hash_map.clear();
    println!("{:?}", hash_map);
}