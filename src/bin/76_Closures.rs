struct City {
    city: String,
    population: i32,
}

fn sort_population(cities: &mut Vec<City>) {
    cities.sort_by_key(|city: &City| city.population);
    // cities.sort_by_key(population);
    // cities.sort_by(|a, b| a.population.cmp(&b.population));
}

fn population(city: &City) -> i32 {
    city.population
}

fn main() {
    let city1 = City {
        city: "London".to_string(),
        population: 8_000_000,
    };
    let city2 = City {
        city: "Paris".to_string(),
        population: 2_000_000,
    };
    let city3 = City {
        city: "Berlin".to_string(),
        population: 3_000_000,
    };
    let mut cities = vec![city1, city2, city3];
    sort_population(&mut cities);
    for city in cities {
        println!("{}: {}", city.city, city.population);
    }
}