fn insertion_sort(v: &mut Vec<i32>) {
    for i in 0..v.len() {
        let mut left_pointer = i;
        let mut right_pointer = i + 1;
        while left_pointer > 0 {
            left_pointer -= 1;
            right_pointer -= 1;
            if v[left_pointer] > v[right_pointer] {
                v.swap(left_pointer, right_pointer);
            } else {
                break;
            }
        }
    }
}

fn main() {
    let mut v1 = vec![2, 1, 3, 4, 10, 9];
    println!("Before: {:?}", v1);
    insertion_sort(&mut v1);
    println!("After: {:?}", v1);

    let mut v2 = vec![4, 3, 1, 2];
    println!("Before: {:?}", v2);
    insertion_sort(&mut v2);
    println!("After: {:?}", v2);

    let mut v3 = vec![5, 10, 1, 4, 11];
    println!("Before: {:?}", v3);
    insertion_sort(&mut v3);
    println!("After: {:?}", v3);
}