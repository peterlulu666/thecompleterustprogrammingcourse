trait Overview {
    fn overview(&self) -> String;
}

struct Course {
    name: String,
    number: i32,
}

impl Overview for Course {
    fn overview(&self) -> String {
        format!("{}: {}", self.name, self.number)
    }
}

fn print_overview(overview: &impl Overview) {
    println!("{}", overview.overview());
}

fn main() {
    let course = Course {
        name: "Rust".to_string(),
        number: 101,
    };
    print_overview(&course);
}