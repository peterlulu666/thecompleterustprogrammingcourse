fn main() {
    let mut number = vec![1, 2, 3];
    number.push(4);
    println!("{:?}", number);
    number.pop();
    println!("{:?}", number);
    let mut number2 = Vec::new();
    number2.push("hi");
    number2.push("Hello");
    println!("{:?}", number2);
    number2.reverse();
    println!("{:?}", number2);
    let mut number3 = Vec::<i32>::with_capacity(2);
    println!("{:?}", number3.capacity());
    let mut number4:Vec<i32> = (1..10).collect();
    println!("{:?}", number4);
}