enum Pet {
    Dog,
    Cat,
    Pig,
}

impl Pet {
    fn speak(&self) -> &'static str {
        match *self {
            Pet::Dog => "woof",
            Pet::Cat => "meow",
            Pet::Pig => "oink",
        }
    }
}

#[derive(Debug)]
enum IpAddrKind {
    V4(String),
    V6,
}

#[derive(Debug)]
struct IpAddr {
    kind: IpAddrKind,
    address: String,
}

fn main() {
    let pig = Pet::Pig;
    println!("The pig says {}", pig.speak());
    let home = IpAddrKind::V4(String::from("127.0.0.1"));
    let loopack = IpAddr {
        kind: IpAddrKind::V6,
        address: String::from("::1"),
    };
    println!("Home: {:?}, Loopback: {:?}", home, loopack);
}