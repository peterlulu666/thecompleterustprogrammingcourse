fn main() {
    let plus_one = |x: i32| -> i32 { x + 1 };
    println!("1 + 1 = {}", plus_one(1));
    let plus = |x, y| x + y;
    println!("2 + 2 = {}", plus(2, 2));
    let print = |x| x;
    println!("3 = {}", print(3));
}