fn selection_sort(v: &mut Vec<i32>) {
    for i in 0..v.len() - 1 {
        let mut current_pointer = i;
        let mut left_pointer = i;
        for j in left_pointer + 1..v.len() {
            let mut right_pointer = j;
            let mut left_value = v[left_pointer];
            let mut right_value = v[right_pointer];
            if right_value < left_value {
                left_pointer = right_pointer;
            }
        }
        v.swap(current_pointer, left_pointer);
    }
}

fn main() {
    let mut v = vec![2, 1, 3, 4, 10, 9];
    println!("Before: {:?}", v);
    selection_sort(&mut v);
    println!("After: {:?}", v);

    let mut v2 = vec![4, 3, 1, 2];
    println!("Before: {:?}", v2);
    selection_sort(&mut v2);
    println!("After: {:?}", v2);

    let mut v3 = vec![5, 10, 1, 4, 11];
    println!("Before: {:?}", v3);
    selection_sort(&mut v3);
    println!("After: {:?}", v3);
}