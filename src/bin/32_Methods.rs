struct Square {
    width: i32,
    height: i32,
}

impl Square {
    fn area(&self) -> i32 {
        return self.width * self.height;
    }

    fn what_is_width(&self) -> i32 {
        return self.width;
    }

    fn change_width(&mut self, input_width: i32) {
        self.width = input_width;
    }
}

fn main() {
let mut square = Square {
        width: 10,
        height: 10,
    };

    println!("The area of the square is {}", square.area());
    println!("The width of the square is {}", square.what_is_width());
    square.change_width(20);
    println!("The width of the square is {}", square.what_is_width());
}