use ::std::collections::LinkedList;

pub struct Queue<T> {
    list: LinkedList<T>,
}

impl<T> Queue<T> {
    pub fn new() -> Self {
        Queue { list: LinkedList::new() }
    }

    pub fn enqueue(&mut self, item: T) {
        self.list.push_back(item);
    }

    pub fn dequeue(&mut self) -> Option<T> {
        self.list.pop_front()
    }

    pub fn peek(&self) -> Option<&T> {
        self.list.front()
    }

    pub fn is_empty(&self) -> bool {
        self.list.is_empty()
    }

    pub fn len(&self) -> usize {
        self.list.len()
    }
}
fn main() {
    let mut queue = Queue::new();
    queue.enqueue(1);
    queue.enqueue(2);
    queue.enqueue(3);
    queue.enqueue(4);
    queue.enqueue(5);
    println!("Queue length: {}", queue.len());
    println!("Queue is empty: {}", queue.is_empty());
    println!("Queue peek: {:?}", queue.peek());
    println!("Queue dequeue: {:?}", queue.dequeue());
    println!("Queue dequeue: {:?}", queue.dequeue());
    println!("Queue dequeue: {:?}", queue.dequeue());
    println!("Queue dequeue: {:?}", queue.dequeue());
    println!("Queue dequeue: {:?}", queue.dequeue());
    println!("Queue dequeue: {:?}", queue.dequeue());
    println!("Queue length: {}", queue.len());
    println!("Queue is empty: {}", queue.is_empty());
    println!("Queue peek: {:?}", queue.peek());
}