use std::sync::Arc;

fn main() {
    let rc1 = Arc::new(5);
    let rc2 = rc1.clone();
    std::thread::spawn(move || {
        println!("rc1 = {}", rc1);
        println!("rc2 = {}", rc2);
    });

    std::thread::sleep(std::time::Duration::from_secs(1));
}