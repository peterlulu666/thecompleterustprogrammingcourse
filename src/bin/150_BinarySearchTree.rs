use ::std::ops::Deref;
use ::std::cmp::Ordering;

#[derive(Debug)]
pub struct BinarySearchTree<T>
    where
        T: Ord,
{
    value: Option<T>,
    left: Option<Box<BinarySearchTree<T>>>,
    right: Option<Box<BinarySearchTree<T>>>,
}

impl<T> Default for BinarySearchTree<T>
    where
        T: Ord,
{
    fn default() -> Self {
        Self::new()
    }
}

impl<T> BinarySearchTree<T>
    where
        T: Ord,
{
    pub fn new() -> Self {
        Self {
            value: None,
            left: None,
            right: None,
        }
    }

    pub fn insert(&mut self, value: T) {
        if self.value.is_none() {
            self.value = Some(value);
        } else {
            match &self.value {
                None => (),
                Some(key) => {
                    let target_node = if value < *key {
                        &mut self.left
                    } else {
                        &mut self.right
                    };

                    match target_node {
                        Some(ref mut node) => node.insert(value),
                        None => {
                            let mut new_node = Self::new();
                            new_node.insert(value);
                            *target_node = Some(Box::new(new_node));
                        }
                    }
                }
            }
        }
    }

    pub fn search(&self, value: &T) -> bool {
        match &self.value {
            Some(key) => {
                match key.cmp(value) {
                    Ordering::Equal => {
                        true
                    }
                    Ordering::Greater => {
                        match &self.left {
                            Some(ref node) => node.search(value),
                            None => false,
                        }
                    }
                    Ordering::Less => {
                        match &self.right {
                            Some(ref node) => node.search(value),
                            None => false,
                        }
                    }
                }
            }
            None => false,
        }
    }

    pub fn minimum(&self) -> Option<&T> {
        match &self.left {
            Some(ref node) => node.minimum(),
            None => self.value.as_ref(),
        }
    }

    pub fn maximum(&self) -> Option<&T> {
        match &self.right {
            Some(ref node) => node.maximum(),
            None => self.value.as_ref(),
        }
    }

    pub fn floor(&self, value: &T) -> Option<&T> {
        match &self.value {
            Some(key) => {
                match key.cmp(value) {
                    Ordering::Equal => {
                        Some(key)
                    }
                    Ordering::Greater => {
                        match &self.left {
                            Some(ref node) => node.floor(value),
                            None => None,
                        }
                    }
                    Ordering::Less => {
                        match &self.right {
                            Some(ref node) => {
                                match node.floor(value) {
                                    Some(v) => Some(v),
                                    None => Some(key),
                                }
                            }
                            None => Some(key),
                        }
                    }
                }
            }
            None => None,
        }
    }

    pub fn ceiling(&self, value: &T) -> Option<&T> {
        match &self.value {
            Some(key) => {
                match key.cmp(value) {
                    Ordering::Equal => {
                        Some(key)
                    }
                    Ordering::Greater => {
                        match &self.left {
                            Some(ref node) => {
                                match node.ceiling(value) {
                                    Some(v) => Some(v),
                                    None => Some(key),
                                }
                            }
                            None => Some(key),
                        }
                    }
                    Ordering::Less => {
                        match &self.right {
                            Some(ref node) => node.ceiling(value),
                            None => None,
                        }
                    }
                }
            }
            None => None,
        }
    }
}

struct BinarySearchTreeIterator<'a, T>
    where
        T: Ord,
{
    stack: Vec<&'a BinarySearchTree<T>>,
}

impl<'a, T> BinarySearchTreeIterator<'a, T>
    where
        T: Ord,
{
    pub fn new(tree: &BinarySearchTree<T>) -> BinarySearchTreeIterator<T> {
        let mut iter = BinarySearchTreeIterator {
            stack: vec![tree],
        };
        iter.stack_push_left();
        iter
    }

    fn stack_push_left(&mut self) {
        while let Some(child) = self.stack.last().unwrap().left.as_ref() {
            self.stack.push(child);
        }
    }
}

impl<'a, T> Iterator for BinarySearchTreeIterator<'a, T>
    where
        T: Ord,
{
    type Item = &'a T;

    fn next(&mut self) -> Option<&'a T> {
        if self.stack.is_empty() {
            return None;
        } else {
            let node = self.stack.pop().unwrap();
            if node.right.is_some() {
                self.stack.push(node.right.as_ref().unwrap());
                self.stack_push_left();
            }
            node.value.as_ref()
        }
    }
}


fn create() -> BinarySearchTree<i32> {
    let mut tree = BinarySearchTree::new();
    tree.insert(5);
    tree.insert(3);
    tree.insert(7);
    tree.insert(2);
    tree.insert(4);
    tree.insert(6);
    tree.insert(8);
    tree
}

fn iterator() {
    let tree = create();
    let mut iter = BinarySearchTreeIterator::new(&tree);
    while let Some(value) = iter.next() {
        println!("{}", value);
    }
}

fn search() {
    let tree = create();
    println!("search 5: {}", tree.search(&5));
    println!("search 3: {}", tree.search(&3));
    println!("search 7: {}", tree.search(&7));
    println!("search 2: {}", tree.search(&2));
    println!("search 4: {}", tree.search(&4));
    println!("search 6: {}", tree.search(&6));
    println!("search 8: {}", tree.search(&8));
    println!("search 9: {}", tree.search(&9));
}

fn minimum() {
    let tree = create();
    println!("minimum: {}", tree.minimum().unwrap());
}

fn maximum() {
    let tree = create();
    println!("maximum: {}", tree.maximum().unwrap());
}

fn floor() {
    let tree = create();
    println!("floor 5: {}", tree.floor(&5).unwrap());
    println!("floor 3: {}", tree.floor(&3).unwrap());
    println!("floor 7: {}", tree.floor(&7).unwrap());
    println!("floor 2: {}", tree.floor(&2).unwrap());
    println!("floor 4: {}", tree.floor(&4).unwrap());
    println!("floor 6: {}", tree.floor(&6).unwrap());
    println!("floor 8: {}", tree.floor(&8).unwrap());
    println!("floor 9: {:?}", tree.floor(&9));
}

fn ceiling() {
    let tree = create();
    println!("ceiling 5: {}", tree.ceiling(&5).unwrap());
    println!("ceiling 3: {}", tree.ceiling(&3).unwrap());
    println!("ceiling 7: {}", tree.ceiling(&7).unwrap());
    println!("ceiling 2: {}", tree.ceiling(&2).unwrap());
    println!("ceiling 4: {}", tree.ceiling(&4).unwrap());
    println!("ceiling 6: {}", tree.ceiling(&6).unwrap());
    println!("ceiling 8: {}", tree.ceiling(&8).unwrap());
    println!("ceiling 1: {:?}", tree.ceiling(&1));
}

fn main() {
    let tree = create();
    println!("{:?}", tree);
    iterator();
    search();
    floor();
    ceiling();
    minimum();
    maximum();
}