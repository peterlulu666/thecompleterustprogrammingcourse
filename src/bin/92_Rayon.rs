use rayon::prelude::*;
use num::{BigUint, One};
use std::time::Instant;

fn factorial(n: u32) -> BigUint {
    if n == 0 || n == 1 {
        return BigUint::one();
    }
    (2..n).map(BigUint::from).reduce(|a, b| a * b).unwrap()
}

fn multi_factorial(n: u32) -> BigUint {
    if n == 0 || n == 1 {
        return BigUint::one();
    }
    (2..n).into_par_iter().map(BigUint::from).reduce(|| BigUint::one(), |a, b| a * b)
}

fn main() {
    let now = Instant::now();
    let n = 10000;
    let f = factorial(n);
    println!("{}! = {}", n, f);

    let now = Instant::now();
    let n = 10000;
    let f = multi_factorial(n);
    println!("{}! = {}", n, f);
}