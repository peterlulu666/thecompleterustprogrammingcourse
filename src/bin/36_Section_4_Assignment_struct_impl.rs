struct Car {
    mpg: i32,
    color: String,
    top_speed: i32,
}

impl Car {
    fn set_mpg(&mut self, new_mpg: i32) {
        self.mpg = new_mpg;
    }
    fn set_color(&mut self, new_color: String) {
        self.color = new_color;
    }
    fn set_top_speed(&mut self, new_top_speed: i32) {
        self.top_speed = new_top_speed;
    }
}

fn main() {
    let mut car = Car {
        mpg: 20,
        color: String::from("red"),
        top_speed: 200,
    };

    car.set_mpg(30);
    car.set_color(String::from("blue"));
    car.set_top_speed(300);

    println!("Car: {} {} {}", car.mpg, car.color, car.top_speed);
}