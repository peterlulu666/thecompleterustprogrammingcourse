struct Car {
    mpg: f64,
    color: String,
    top_speed: i32,
}

struct Motorcycle {
    mpg: f64,
    color: String,
    top_speed: i32,
}

trait Properties {
    fn set_mpg(&mut self, mpg: f64);
    fn set_color(&mut self, color: String);
    fn set_top_speed(&mut self, top_speed: i32);
}

impl Properties for Car {
    fn set_mpg(&mut self, mpg: f64) {
        self.mpg = mpg;
    }

    fn set_color(&mut self, color: String) {
        self.color = color;
    }

    fn set_top_speed(&mut self, top_speed: i32) {
        self.top_speed = top_speed;
    }
}

impl Properties for Motorcycle {
    fn set_mpg(&mut self, mpg: f64) {
        self.mpg = mpg;
    }

    fn set_color(&mut self, color: String) {
        self.color = color;
    }

    fn set_top_speed(&mut self, top_speed: i32) {
        self.top_speed = top_speed;
    }
}

fn print_info<T: std::fmt::Debug>(info: T) {
    println!("{:?}", info);
}

fn main() {
    let car = Car {
        mpg: 20.0,
        color: String::from("Red"),
        top_speed: 200,
    };

    let motorcycle = Motorcycle {
        mpg: 40.0,
        color: String::from("Blue"),
        top_speed: 150,
    };

    println!("Car: {} {} {}", car.mpg, car.color, car.top_speed);
    println!("Motorcycle: {} {} {}", motorcycle.mpg, motorcycle.color, motorcycle.top_speed);

    print_info(vec![1, 2, 3]);
    print_info("string");
    print_info(1);
}