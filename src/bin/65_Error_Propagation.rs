use std::fs::{File, rename};
use std::io::Error;

fn open_file() -> Result<File, Error> {
    let f = File::open("hello.txt")?;
    Ok(f)
}

fn rename_file() -> Result<(), Error> {
    let file = rename("hello.txt", "hello2.txt")?;
    Ok(file)
}

fn main() {
    let f = open_file();
    f.unwrap();
    rename_file().unwrap();
}