use std::rc::Rc;

fn main() {
    // variable on stack
    let number1 = 5;
    // variable on heap
    let number2 = Box::new(100);
    // multiply
    println!("number1 * number2 = {}", number1 * *number2);
    // Create string
    let string1 = String::from("value");
    // Create smart pointer point to string
    let rc: Rc<String> = Rc::new(string1);
    // Print how many smart pointer point to string
    println!("rc = {}, rc = {}", Rc::strong_count(&rc), rc);
    // Clone smart pointer
    let rc2 = Rc::clone(&rc);
    // Print how many smart pointer point to string
    println!("rc = {}, rc2 = {}", Rc::strong_count(&rc), Rc::strong_count(&rc2));
}