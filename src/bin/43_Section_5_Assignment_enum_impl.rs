enum Shape {
    Triangle,
    Rectangle,
    Pentagon,
    Octagon,
}

impl Shape {
    fn corner(&self) -> i32 {
        match self {
            Shape::Triangle => 3,
            Shape::Rectangle => 4,
            Shape::Pentagon => 5,
            Shape::Octagon => 8,
        }
    }
}

fn main() {
    let triangle = Shape::Triangle;
    let rectangle = Shape::Rectangle;
    let pentagon = Shape::Pentagon;
    let octagon = Shape::Octagon;
    println!("Triangle has {} corners", triangle.corner());
    println!("Rectangle has {} corners", rectangle.corner());
    println!("Pentagon has {} corners", pentagon.corner());
    println!("Octagon has {} corners", octagon.corner());

}