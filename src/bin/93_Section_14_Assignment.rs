use std::time::Instant;
use rayon::prelude::*;

fn fib(n: u64) -> u64 {
    if n < 2 {
        n
    } else {
        fib(n - 1) + fib(n - 2)
    }
}

fn fib_rayon(n: u64) -> u64 {
    if n <= 1 {
        return n;
    }
    let (a, b) = rayon::join(|| fib(n - 1), || fib(n - 2));
    a + b
}

fn main() {
    let start = Instant::now();
    let x = fib(40);
    let duration = start.elapsed();
    println!("{} in {:?}", x, duration);

    println!("Running Rayon");

    let start = Instant::now();
    let x = fib_rayon(40);
    let duration = start.elapsed();
    println!("{} in {:?}", x, duration);
}