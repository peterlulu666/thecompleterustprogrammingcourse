fn bubble_sort(arr: &mut [i32]) {
    let mut swapped = true;
    while swapped {
        swapped = false;
        for i in 1..arr.len() {
            if arr[i - 1] > arr[i] {
                arr.swap(i - 1, i);
                swapped = true;
            }
        }
    }
}

fn main() {
    let mut v1 = vec![2, 1, 3, 4, 10, 9];
    println!("Before: {:?}", v1);
    bubble_sort(&mut v1);
    println!("After: {:?}", v1);

    let mut v2 = vec![4, 3, 1, 2];
    println!("Before: {:?}", v2);
    bubble_sort(&mut v2);
    println!("After: {:?}", v2);

    let mut v3 = vec![5, 10, 1, 4, 11];
    println!("Before: {:?}", v3);
    bubble_sort(&mut v3);
    println!("After: {:?}", v3);
}