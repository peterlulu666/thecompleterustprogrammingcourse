struct Point<U,T> {
    x: U,
    y: T,
}

fn main() {
    let p = Point { x: 5, y: 10.4 };
    println!("p.x = {}, p.y = {}", p.x, p.y);
}