use std::collections::HashSet;

fn main() {
    let mut hash_set = HashSet::new();
    hash_set.insert(1);
    hash_set.insert(2);
    hash_set.insert(3);
    println!("hash_set: {:?}", hash_set);
    println!("len: {}", hash_set.len());
    for i in hash_set.iter() {
        println!("iter {}", i);
    }

    let mut hash_set2 = HashSet::new();
    hash_set2.insert(100);
    hash_set2.insert(200);
    hash_set2.insert(300);
    hash_set2.insert(3);
    for i in hash_set2.intersection(&hash_set) {
        println!("intersection {}", i);
    }
    let intersection: HashSet<_> = hash_set2.intersection(&hash_set).collect();
    println!("intersection: {:?}", intersection);
    let intersection2 = &hash_set2 & &hash_set;
    println!("intersection2: {:?}", intersection2);
    let union: HashSet<_> = hash_set2.union(&hash_set).collect();
    println!("union: {:?}", union);
    let union2 = &hash_set2 | &hash_set;
    println!("union2: {:?}", union2);
}