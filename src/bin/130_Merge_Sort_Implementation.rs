fn merge_sort(v: &mut Vec<i32>) -> Vec<i32> {
    if v.len() <= 1 {
        return v.clone();
    }
    let mid_index = v.len() / 2;
    let mut split_one = v[..mid_index].to_vec();
    let mut split_two = v[mid_index..].to_vec();
    let mut list_one = merge_sort(&mut split_one);
    let mut list_two = merge_sort(&mut split_two);
    return merge(&mut list_one, &mut list_two);
}

fn merge(list_one: &mut Vec<i32>, list_two: &mut Vec<i32>) -> Vec<i32> {
    let mut merged_list = Vec::new();
    while list_one.len() > 0 && list_two.len() > 0 {
        if list_one[0] < list_two[0] {
            merged_list.push(list_one.remove(0));
        } else {
            merged_list.push(list_two.remove(0));
        }
    }
    while list_one.len() > 0 {
        merged_list.push(list_one.remove(0));
    }
    while list_two.len() > 0 {
        merged_list.push(list_two.remove(0));
    }
    return merged_list;
}

fn main() {
    let mut v1 = vec![2, 1, 3, 4, 10, 9];
    println!("Before: {:?}", v1);
    println!("After: {:?}", merge_sort(&mut v1));

    let mut v2 = vec![4, 3, 1, 2];
    println!("Before: {:?}", v2);
    println!("After: {:?}", merge_sort(&mut v2));

    let mut v3 = vec![5, 10, 1, 4, 11];
    println!("Before: {:?}", v3);
    println!("After: {:?}", merge_sort(&mut v3));
}