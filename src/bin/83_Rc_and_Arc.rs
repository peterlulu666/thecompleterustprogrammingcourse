use std::rc::Rc;

fn main() {
    let s1 = Rc::new("Pointer".to_string());
    let s2 = s1.clone();
    let s3 = s1.clone();
    println!("{} {} {}", s1.contains("Point"), s2, s3.contains("ter"));
}