use std::sync::mpsc::channel;

fn main() {
    let (transmitter, receiver) = channel();
    let tx = transmitter.clone();
    // let val = String::from("Transmitter");
    // std::thread::spawn(move || {
    //     transmitter.send(val).unwrap();
    // });
    // let received = receiver.recv().unwrap();
    // println!("Received: {}", received);
    std::thread::spawn(move || {
        let vec = vec![
            "Hello".to_string(),
            "from".to_string(),
            "the".to_string(),
            "transmitter".to_string()];
        for val in vec {
            transmitter.send(val).unwrap();
        }
    });

    std::thread::spawn(move || {
        let vec = vec![
            "Hello".to_string(),
            "from".to_string(),
            "the".to_string(),
            "clone transmitter".to_string()];
        for val in vec {
            tx.send(val).unwrap();
        }
    });

    for rec in receiver {
        println!("Received: {}", rec);
    }
}