trait Overview {
    fn overview(&self) -> String;
}

struct Course {
    course_name: String,
    course_number: u32,
}

struct AnotherCourse {
    course_name: String,
    course_number: u32,
}

impl Overview for Course {
    fn overview(&self) -> String {
        format!("{} {}", self.course_name, self.course_number)
    }
}

impl Overview for AnotherCourse {
    fn overview(&self) -> String {
        format!("{} {}", self.course_name, self.course_number)
    }
}

fn main() {
    let course = Course {
        course_name: "Rust".to_string(),
        course_number: 101,
    };

    println!("{}", course.overview());

    let another_course = AnotherCourse {
        course_name: "Java".to_string(),
        course_number: 102,
    };

    println!("{}", another_course.overview());
}