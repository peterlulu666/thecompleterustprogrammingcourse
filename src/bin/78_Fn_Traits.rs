fn main() {
    let y = 5;
    let plus_y = |x: i32| x + y;
    let copy_closure = plus_y;
    println!("{}", plus_y(5));
    println!("{:?}", copy_closure(5));
}