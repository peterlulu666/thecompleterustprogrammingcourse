struct Node {
    data: i32,
    next: Option<Box<Node>>,
}

struct LinkedList {
    head: Option<Box<Node>>,
}

impl LinkedList {
    fn new() -> Self {
        LinkedList { head: None }
    }

    fn push(&mut self, data: i32) {
        let new_node = Box::new(Node {
            data,
            next: self.head.take(),
        });
        self.head = Some(new_node);
    }

    fn pop(&mut self) -> Option<i32> {
        self.head.take().map(|node| {
            self.head = node.next;
            node.data
        })
    }

    fn peek(&self) -> Option<&i32> {
        self.head.as_ref().map(|node| &node.data)
    }

    fn print_list(&self) {
        let mut current = &self.head;
        while let Some(node) = current {
            print!("{} -> ", node.data);
            current = &node.next;
        }
        println!("None");
    }
}

fn main() {
    let mut list = LinkedList::new();
    println!("pop: {:?}", list.pop() == None);
    list.push(1);
    list.push(2);
    list.push(3);
    println!("pop: {:?}", list.pop() == Some(3));
    println!("pop: {:?}", list.pop() == Some(2));
    list.push(4);
    list.push(5);
    println!("pop: {:?}", list.pop() == Some(5));
    println!("pop: {:?}", list.pop() == Some(4));
    println!("pop: {:?}", list.pop() == Some(1));
    println!("pop: {:?}", list.pop() == None);
    list.push(1);
    list.push(2);
    list.push(3);
    println!("peek: {:?}", list.peek() == Some(&3));
    list.print_list();
}