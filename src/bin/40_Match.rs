fn plus_one(x: Option<i32>) -> Option<i32> {
    match x {
        None => None,
        Some(i) => Some(i + 1),
    }
}
fn what_pet(pet: &str) {
    match pet {
        "cat" => println!("Cat"),
        "dog" => println!("Dog"),
        "pig" => println!("Pig"),
        _ => println!("Other"),
    }
}
fn main() {
    let five = Some(5);
    let six = plus_one(five);
    println!("{:?}", six);
    let none = plus_one(None);
    println!("{:?}", none);
    what_pet("cat");
    what_pet("dog");
    what_pet("pig");
}