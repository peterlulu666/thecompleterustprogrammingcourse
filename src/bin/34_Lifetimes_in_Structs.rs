struct MyStruct<'a> {
    x: &'a str,
}

fn main() {
    let str1 = String::from("This is String");
    let x = MyStruct { x: str1.as_str() };
}