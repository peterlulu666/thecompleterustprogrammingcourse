fn main() {
    let some_number = Some(5);
    let some_string = Some("a string");
    let nothing:Option<i32> = None;
    println!("{:?} {:?} {:?}", some_number, some_string, nothing);
    let x = Some(5);
    println!("x {:?} unwrap {}", x, x.unwrap());
    let y = Some(5);
    println!("y {:?} unwrap {}", y, y.unwrap());
    println!("x + y = {}", x.unwrap() + y.unwrap());
}