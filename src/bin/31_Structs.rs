struct User {
    active: bool,
    username: String,
    sign_in_count: u64,
}

struct Coordinates(i32, i32, i32);

struct UnitStruct;

fn build_user(username: String) -> User {
    return User {
        username: username,
        active: true,
        sign_in_count: 1,
    };
}

fn main() {
    let user1 = User {
        username: String::from("user1"),
        active: true,
        sign_in_count: 1,
    };
    println!("User1: {}", user1.username);

    let user2 = build_user(String::from("user2"));
    println!("User2: {}", user2.username);

    let coords = Coordinates(1, 2, 3);
    println!("Coordinates: {}, {}, {}", coords.0, coords.1, coords.2);
}