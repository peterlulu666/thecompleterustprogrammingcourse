fn main() {
    let one = 1;
    if one > 10 {
        println!("true");
    } else if one == 1 {
        println!("Equal");
    } else {
        println!("false");
    }

    // let mut outer_count = 0;
    // 'counter: loop {
    //     println!("outer_count: {}", outer_count);
    //     let mut inner_count = 5;
    //     loop {
    //         println!("inner_count: {}", inner_count);
    //         if inner_count == 3 {
    //             break;
    //         }
    //
    //         if outer_count == 3 {
    //             break 'counter;
    //         }
    //
    //         inner_count = inner_count - 1;
    //     }
    //     outer_count = outer_count + 1;
    // }

    let mut outer_count = 0;
    loop {
        if outer_count == 4 {
            break;
        }
        println!("outer_count: {}", outer_count);
        let mut inner_count = 5;
        loop {
            println!("inner_count: {}", inner_count);
            if inner_count == 3 {
                break;
            }
            inner_count = inner_count - 1;
            if outer_count == 3 {
                break;
            }
        }
        outer_count = outer_count + 1;
    }

    let mut number = 0;
    while number < 10 {
        println!("number: {}", number);
        number = number + 1;
    }

    let v: Vec<i32> = (0..10).collect();
    for element in v {
        println!("element: {}", element);
    }

    for element in (0..10).rev() {
        println!("rev element: {}", element);
    }
}