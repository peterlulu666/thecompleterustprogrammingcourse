use async_std::{fs::File, io, prelude::*, task};

async fn read_file(path: &str) -> io::Result<String> {
    let mut file = File::open(path).await?;
    let mut contents = String::new();
    file.read_to_string(&mut contents).await?;
    Ok(contents)
}

fn main() {
    let task = task::spawn(async {
        let contents = read_file("hello2.txt").await;
        match contents {
            Ok(K) => println!("File contents: {}", K),
            Err(K) => println!("Error: {}", K),
        }
    });

    println!("Task spawned");
    task::block_on(task);
    println!("Complete task");
}