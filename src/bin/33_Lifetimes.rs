fn print_Str<'a, 'b>(s: &'a str, t: &'b str) {
    println!("s = {}, t = {}", s, t);
}
fn main() {
    let s = "Hello";
    let t = "World";
    print_Str(s, t);
}