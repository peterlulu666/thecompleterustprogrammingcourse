use std::collections::{HashSet, VecDeque};
use std::env::join_paths;

pub fn breadth_first_search(graph: &Graph, root: Vertex, target: Vertex) -> Option<Vec<u8>> {
    let mut queue = VecDeque::new();
    let mut visited = HashSet::new();
    let mut path = Vec::new();

    queue.push_back(root);
    visited.insert(root);

    while let Some(vertex) = queue.pop_front() {
        path.push(vertex.value());

        if vertex == target {
            return Some(path);
        }

        for neighbor in vertex.neighbors(graph) {
            if !visited.contains(&neighbor) {
                queue.push_back(neighbor);
                visited.insert(neighbor);
            }
        }
    }
    None
}

// pub fn breadth_first_search(graph: &Vec<Vec<usize>>, start: usize) -> Vec<usize> {
//     let mut visited = HashSet::new();
//     let mut queue = VecDeque::new();
//     let mut order = Vec::new();
//
//     queue.push_back(start);
//     visited.insert(start);
//
//     while let Some(node) = queue.pop_front() {
//         order.push(node);
//
//         for &neighbor in &graph[node] {
//             if !visited.contains(&neighbor) {
//                 visited.insert(neighbor);
//                 queue.push_back(neighbor);
//             }
//         }
//     }
//
//     order
// }

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub struct Vertex(u8);

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub struct Edge(u8, u8);

#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub struct Graph {
    vertices: Vec<Vertex>,
    edges: Vec<Edge>,
}

impl From<u8> for Vertex {
    fn from(v: u8) -> Self {
        Vertex(v)
    }
}

impl Vertex {
    pub fn value(&self) -> u8 {
        self.0
    }

    pub fn neighbors(&self, graph: &Graph) -> VecDeque<Vertex> {
        graph.edges.iter()
            .filter(|e| e.0 == self.0)
            .map(|e| e.1.into())
            .collect()
    }
}

impl From<(u8, u8)> for Edge {
    fn from(e: (u8, u8)) -> Self {
        Edge(e.0, e.1)
    }
}

impl Graph {
    pub fn new(vertices: Vec<Vertex>, edges: Vec<Edge>) -> Self {
        Graph { vertices, edges }
    }
}

fn main() {
    let vertices = vec![1, 2, 3, 4, 5, 6, 7];
    let edges = vec![(1, 2), (1, 3), (2, 4), (2, 5), (3, 6), (3, 7)];
    let root = 1;
    let target = 7;
    let graph = Graph::new(vertices.into_iter().map(|v| v.into()).collect(),
                           edges.into_iter().map(|e| e.into()).collect());
    println!("{:?}", breadth_first_search(&graph, root.into(), target.into()));

    // let graph = Graph::new(
    //     vec![0, 1, 2, 3, 4, 5, 6, 7, 8].into_iter().map(Vertex::from).collect(),
    //     vec![(0, 1), (0, 2), (0, 3), (1, 4), (1, 5), (2, 6), (2, 7), (3, 8)].into_iter().map(Edge::from).collect(),
    // );
    //
    // let order = breadth_first_search(&graph.vertices.iter().map(|v| v.neighbors(&graph).into_iter().map(|v| v.value() as usize).collect()).collect(), 0);
    //
    // println!("{:?}", order);
}