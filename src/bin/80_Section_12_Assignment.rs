fn main() {
    let vec = vec![1, 2, 3, 4, 5];
    let time_ten: Vec<i32> = vec.iter().map(|x| x * 10).collect();
    println!("{:?}", time_ten);
}