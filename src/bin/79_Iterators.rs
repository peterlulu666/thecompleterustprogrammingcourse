#[derive(Debug)]
struct Item {
    name: String,
}

fn check_inventory(items: Vec<Item>, product: String) -> Vec<Item> {
    items.into_iter()
        .filter(|item| item.name == product)
        .collect()
}

struct Range {
    start: i32,
    end: i32,
}

impl Iterator for Range {
    type Item = i32;

    fn next(&mut self) -> Option<Self::Item> {
        if self.start < self.end {
            let result = Some(self.start);
            self.start += 1;
            result
        } else {
            None
        }
    }
}

fn main() {
    let v = vec![1, 2, 3];
    for i in v.iter() {
        println!("iter: {}", i);
    }
    println!("v: {:?}", v);

    let v2 = vec![1, 2, 3];
    let mut v2_iter = v2.into_iter();
    while let Some(i) = v2_iter.next() {
        println!("into_iter: {}", i);
    }

    let mut item = Vec::new();
    item.push(Item { name: "apple".to_string() });
    item.push(Item { name: "banana".to_string() });
    item.push(Item { name: "orange".to_string() });

    let check = check_inventory(item, "apple".to_string());
    println!("check: {:?}", check);

    let mut range = Range { start: 0, end: 10 };
    for r in range {
        println!("range: {}", r);
    }
}